# Contribute

1. Fork the [repository](https://gitlab.com/Louson/python-musicbrainzez)
   on Github.
2. Make and test whatever changes you desire.
3. Signoff and commit your changes using ``git commit -s``.
4. Send a pull request.
