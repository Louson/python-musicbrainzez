#! /usr/bin/python

import musicbrainzez
from musicbrainzez import musicbrainz

musicbrainz.set_useragent("testapp", "0.1")

client_id = 'GGrj8EudItO7b5dT9j-q2dTUqvGI-CSm'
client_secret = 'JIQfdxnFNX0N8UFntMNllP6i2jUozKzx'
scope = ['profile', 'collection', 'submit_barcode']

handler = musicbrainz.oauth_new_handler(client_id, scope=scope)

# For testing purpose
token=""
refresh=""
# end test

if not token:
    url = musicbrainz.oauth_get_authorization_url(handler)
    if not url:
        print("| No url")
        exit()
    print(url[0])

    code=input("Insert the code\n")

    print("| Get tokens")
    authorization = musicbrainz.oauth_get_authorization(handler, client_secret, code)
    if not authorization:
        print("| No token received")
        exit()
    print(authorization)
    token, refresh = (authorization['access_token'], authorization['refresh_token'])
    print("| access token: %s" % token)
    print("| refresh token: %s" % refresh)

try:
    res = musicbrainz._do_mb_query("userinfo", "", [], {},
                                   client_id=client_id, access_token=token)
    print("| ", res)
except:
    print("| Query failed")

print("| Revoke token (next query shall fail)")
musicbrainz.oauth_revoke_token(handler, client_id, client_secret, token)
try:
    res = musicbrainz._do_mb_query("userinfo", "", [], {},
                                   client_id=client_id, access_token=token)
    print("| ", res)
except:
    print("| Query failed")

print("| Refresh token")
authorization = musicbrainz.oauth_refresh_token(handler, client_secret, refresh)
if not authorization:
    print("| No token received")
    exit()
token = authorization['access_token']
print("| access token: %s" % token)

try:
    res = musicbrainz._do_mb_query("userinfo", "", [], [],
                                   client_id=client_id, access_token=token)
    print("| ", res)
except:
    print("| Query failed")
