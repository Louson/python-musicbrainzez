Installation
~~~~~~~~~~~~

Package manager
---------------

If you want the latest stable version of musicbrainzez, the first place to
check is your systems package manager. Being a relatively new library, you
might not be able to find it packaged by your distribution and need to use one
of the alternate installation methods.

PyPI
----

Musicbrainzez may become available on the Python Package Index. This makes installing
it with `pip <http://www.pip-installer.org>`_ as easy as::

    pip install musicbrainzez

Git
---

If you want the latest code or even feel like contributing, the code is
available on `Gitlab <https://gitlab.com/Louson/python-musicbrainzez.git>`_.

You can easily clone the code with git::

    git clone https://gitlab.com/Louson/python-musicbrainzez.git

Now you can start hacking on the code or install it system-wide::

    python setup.py install
