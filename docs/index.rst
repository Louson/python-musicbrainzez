musicbrainzez |release|
========================

`musicbrainzez` implements Python bindings of the `MusicBrainz API`_
(WS/2, NGS).
With this library you can retrieve all kinds of music metadata
from the `MusicBrainz`_ database.

`musicbrainzez` is released under a simplified BSD style license.

.. _`MusicBrainz`: http://musicbrainz.org
.. _`MusicBrainz API`: https://musicbrainz.org/doc/MusicBrainz_API

Contents
--------
.. toctree::

    installation
    usage
    api

.. currentmodule:: musicbrainzez.musicbrainz

Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`
