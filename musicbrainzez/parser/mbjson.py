# This file is part of the musicbrainzez library
# Copyright (C) Louis Rannou and others
# This file is distributed under a BSD-2-Clause type license.
# See the COPYING file for more information.

import logging
import json

from musicbrainzez import compat
from musicbrainzez.util import _unicode

from . import Parser

class JsonParser(Parser):
    def parse(self, body):
        return json.loads(body)
